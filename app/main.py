import time


class Consumer:
    @classmethod
    def read_message(cls):
        print("Message reading...")

    def run(self):
        while True:
            self.read_message()
            time.sleep(1)


if __name__ == '__main__':
    Consumer().run()
